package pages;

import com.google.common.base.Function;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.controls.Keyboard;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SearchPage {

    private static final int EXPLICIT_WAIT_TIME = 10;
    private static final int IMAGES_PER_PAGE = 20;
    private IOSDriver driver;
    private Keyboard keyboard;
    private WebDriverWait webDriverWait;

    @iOSFindBy(xpath = "//XCUIElementTypeSearchField")
    private IOSElement searchTextField;

    @iOSFindBy(xpath = "//XCUIElementTypeSearchField/XCUIElementTypeButton")
    private IOSElement clearBtn;

    @iOSFindBy(xpath = "//XCUIElementTypeCell[*]/XCUIElementTypeStaticText")
    private List<IOSElement> resultTitles;

    public SearchPage(IOSDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
        webDriverWait = new WebDriverWait(driver, EXPLICIT_WAIT_TIME);
    }

    public List<String> getFoundTitles() {
        List<String> resultList = new ArrayList<>(IMAGES_PER_PAGE);
        resultTitles.forEach(title -> resultList.add(title.getText()));
        return resultList;
    }

    public SearchPage enterText(String text) {
        searchTextField.setValue(text);
        return this;
    }

    public boolean isSearchPageOpened() {
        try {
            return searchTextField.isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public SearchPage clearSearchField() {
        if (null != clearBtn && clearBtn.isDisplayed()) {
            clearBtn.click();
        }
        return this;
    }

    public SearchPage pressSearch() {
        if (null == keyboard) {
            keyboard = new Keyboard(driver);
        }
        keyboard.pressSearch();
        return this;
    }

    public SearchPage waitUntilSearchElementsAppears() {
        webDriverWait.pollingEvery(250, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class)
                .ignoring(StaleElementReferenceException.class)
                .withMessage("TimeOut exceeds while waiting results visible on the SearchPage")
                .until((Function<WebDriver, Boolean>) webDriver ->
                        resultTitles.size() == 20);
        return this;
    }

}
