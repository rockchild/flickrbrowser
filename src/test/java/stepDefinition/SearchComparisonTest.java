package stepDefinition;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.ios.IOSDriver;
import pages.SearchPage;
import utils.AppiumDriverBuilder;
import utils.RestHandler;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SearchComparisonTest {
    SearchPage searchPage;
    private IOSDriver driver;

    @Before
    public void setUpTest() {
        driver = AppiumDriverBuilder.getDriver();
        searchPage = new SearchPage(driver);
    }

    @After
    public void tearDownTest() {
        searchPage.clearSearchField();
    }

    @Given("^application is opened$")
    public void applicationIsOpened() {
        assertTrue("Search page wasn't loaded: Search input field is not displayed", searchPage.isSearchPageOpened());
    }

    @When("^I enter (.*) into search field$")
    public void searchText(String text) {
        searchPage.enterText(text);
    }

    @And("^press Search on soft keyboard$")
    public void pressEnter() {
        searchPage.pressSearch().waitUntilSearchElementsAppears();
    }

    @Then("^found results titles equal to retrieved by REST request for (.*)$")
    public void compareResults(String text) {
        List<String> uiTitles = searchPage.getFoundTitles();
        List<String> restTitles = new RestHandler().getResultsTitles(text);

        assertEquals(uiTitles, restTitles);
    }

}
