package models;

/**
 * Model class which is used for deserialization of JSON objects retrieved by Rest requests
 * We have only one field, which is needed as test objective.
 */
public class Image {

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
