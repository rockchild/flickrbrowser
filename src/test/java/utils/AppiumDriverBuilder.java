package utils;

import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class AppiumDriverBuilder {
    private static final String URL = "http://localhost:4723/wd/hub";
    private static final int IMPLICIT_TIMEOUT = 10;

    private static IOSDriver driver = null;
    private DesiredCapabilities caps = new DesiredCapabilities();

    public static IOSDriver getDriver() {
        if (null == driver) {
            return new AppiumDriverBuilder().build();
        }
        return driver;
    }

    public static void quitDriver() {
        if (null != driver) {
            driver.quit();
        }
    }

    private IOSDriver build() {
        PropertiesReader pr = new PropertiesReader();
        for (Object k : pr.getAllKeys()) {
            caps.setCapability((String) k, pr.getPropertyValue((String) k));
        }
        try {
            driver = new IOSDriver(new URL(URL), caps);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.manage().timeouts().implicitlyWait(IMPLICIT_TIMEOUT, TimeUnit.SECONDS);
        return driver;
    }
}