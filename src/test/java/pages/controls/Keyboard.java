package pages.controls;

import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Keyboard class created to perform searching
 */
public class Keyboard {

    @FindBy(name = "Search")
    private WebElement searchKey;

    public Keyboard(WebDriver driver) {
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public void pressSearch() {
        searchKey.click();
    }

}
