package utils;

import io.restassured.path.json.JsonPath;
import models.Image;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Below class is responsible for retrieving data via REST layer
 * In case of the test it return only titles of found results {@link #getResultsTitles(String)}
 */
public class RestHandler {

    // string with a parameter '%s' for GET request
    private final String preformattedUrl = "https://api.flickr.com/services/feeds/photos_public" +
            ".gne?format=json&nojsoncallback=1&tags=%s";

    public List<String> getResultsTitles(String tag) {
        List<Image> images = new ArrayList<>();
        try {
            images = JsonPath.from(new URL(String.format(preformattedUrl, tag))).getList("items", Image.class);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        List<String> titles = new ArrayList<>(images.size());
        images.forEach(image -> titles.add(image.getTitle()));
        return titles;
    }
}
