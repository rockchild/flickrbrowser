
  Feature: To compare search results retrieved by REST request and UI
    I want to search some text via UI and REST request as well
    and compare titles of results

  Scenario Outline: Comparison of REST and UI results

    Given application is opened
    When I enter <text> into search field
    And press Search on soft keyboard
    Then found results titles equal to retrieved by REST request for <text>

    Examples:
    |text         |
    |London       |
    |Peace        |
    |X Generation |