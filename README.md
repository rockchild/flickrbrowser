# FlickrBrowser #

This project was created as an example for showing my speed-code abilities in testing with:

* Java
* Appium
* Cucumber
* Rest-assured

The objective of this test is the comparison of titles of images from a custom Flickr browser iOS application against REST-received titles.