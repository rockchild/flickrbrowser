package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

public class PropertiesReader {
    private String filePath = "src/test/resources/capabilities.properties";
    private Properties prop;

    PropertiesReader() {
        init();
    }

    /**
     * Method initialize the class object using {@link Properties}
     * passing to it specified configurations file
     */
    private void init() {
        prop = new Properties();
        InputStream is = null;
        try {
            is = new FileInputStream(filePath);
            prop.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    Set<Object> getAllKeys() {
        return prop.keySet();
    }

    String getPropertyValue(String key) {
        return prop.getProperty(key);
    }
}
